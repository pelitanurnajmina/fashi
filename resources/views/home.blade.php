@extends('layouts.app')

@section('content')
 <!-- Hero Section Begin -->
<section class="hero-section">
    <div class="hero-items owl-carousel">
        <div class="single-hero-items set-bg" data-setbg="img/herooo.jpg">
        </div>
    </div>
</section>
<!-- Hero Section End -->

<!-- Product Shop Section Begin -->
<section class="product-shop spad page-details">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="blog-sidebar">
                    <div class="search-form">
                        <h4>Search</h4>
                        <form action="#">
                            <input type="text" placeholder="Search . . .  ">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <div class="filter-widget">
                        <h4 class="fw-title">Categories</h4>
                        <ul class="filter-catagories">
                            <li><a href="#">Umum</a></li>
                            <li><a href="#">Kuliner</a></li>
                            <li><a href="#">Pakaian</a></li>
                            <li><a href="#">Otomotif</a></li>
                            <li><a href="#">Kesehatan</a></li>
                            <li><a href="#">Kerajinan Tangan</a></li>
                        </ul>
                    </div>
                    <div class="recent-post">
                        <h4>Recent Post</h4>
                        <div class="recent-blog">
                            <a href="#" class="rb-item">
                                <div class="rb-pic">
                                    <img src="img/vendor/rm.jpg" alt="">
                                </div>
                                <div class="rb-text">
                                    <h6>Rumah Makan Ibu Imas</h6>
                                    <p>Kuliner <span>- May 19, 2020</span></p>
                                </div>
                            </a>
                            <a href="#" class="rb-item">
                                <div class="rb-pic">
                                    <img src="img/vendor/bengkel.jpg" alt="">
                                </div>
                                <div class="rb-text">
                                    <h6>Bengkel Motor Terbaik Padasuka</h6>
                                    <p>Otomotif <span>- May 9, 2020</span></p>
                                </div>
                            </a>
                            <a href="#" class="rb-item">
                                <div class="rb-pic">
                                    <img src="img/vendor/mg.jpg" alt="">
                                </div>
                                <div class="rb-text">
                                    <h6>Sablon Kaos Kustom Bandung</h6>
                                    <p>Pakaian <span>- May 30, 2020</span></p>
                                </div>
                            </a>
                            <a href="#" class="rb-item">
                                <div class="rb-pic">
                                    <img src="img/blog/recent-4.jpg" alt="">
                                </div>
                                <div class="rb-text">
                                    <h6>Outfit Cindy Cimahi</h6>
                                    <p>Pakaian <span>- 06, June 2020</span></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
            	<div class="row">
            	    <div class="col-lg-4 col-sm-6">
            	        <div class="product-item">
            	            <div class="pi-pic">
            	                <img src="img/products/women-large.jpg" alt="">
            	                <div class="icon">
            	                    <i class="icon_heart_alt"></i>
            	                </div>
            	                <ul>
            	                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
            	                </ul>
            	            </div>
            	            <div class="pi-text text-left" style="padding-top: 10px;">
        	                    <h5>Margin Dream</h5>
            	                <div class="catagory-name">Pakaian</div>
            	            </div>
            	        </div>
            	    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/vendor/kopi.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text text-left" style="padding-top: 10px;">
                                <h5>Kedai Kopi ABC</h5>
                                <div class="catagory-name">Kuliner</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/vendor/cell.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text text-left" style="padding-top: 10px;">
                                <h5>Ayub Cell</h5>
                                <div class="catagory-name">Elektronik</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/vendor/shoes.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text text-left" style="padding-top: 10px;">
                                <h5>Ibrahim Shoes</h5>
                                <div class="catagory-name">Pakaian</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/vendor/kue.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text text-left" style="padding-top: 10px;">
                                <h5>Kue Balok Mang Didin</h5>
                                <div class="catagory-name">Kuliner</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/vendor/hijab.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text text-left" style="padding-top: 10px;">
                                <h5>Nur Hijab</h5>
                                <div class="catagory-name">Pakaian</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/vendor/bakso.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="quick-view"><a href="detail">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text text-left" style="padding-top: 10px;">
                                <h5>Bakso Istigfar</h5>
                                <div class="catagory-name">Kuliner</div>
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
        </div>
    </div>
</section>
<!-- Product Shop Section End -->
@endsection