@extends('layouts.umkm')

@push('sec-js')
<script type="text/javascript" src="assets/js/plugins/visualization/echarts/echarts.js"></script>
@endpush

@push('js')
<script type="text/javascript" src="js/chart.js"></script>
@endpush

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Tahun 2020</h5>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>

			<div class="panel-body">
				<div class="chart-container">
					<div class="chart has-fixed-height" id="basic_columns"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection