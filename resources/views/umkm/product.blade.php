@extends('layouts.umkm')

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/man-3.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Jaket</h6>
				<p class="text-muted mb-15 mt-5">Solid purple color Contrast lining with white and orange floral </p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/man-4.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Sweater</h6>
				<p class="text-muted mb-15 mt-5">Long-sleeved custom fit shirt in checked cotton. Buttoned neck.</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/product-1.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Yellow Sweater</h6>
				<p class="text-muted mb-15 mt-5">Buy Designer Shirt at best price of Rs 80/number from Prema</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/product-2.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Soft Sweater</h6>
				<p class="text-muted mb-15 mt-5">Shirt Dengan Daisy Cetak / Kain Katun Mens Kemeja Kasual / Pria Lengan Pendek</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/product-3.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Kemaja Army</h6>
				<p class="text-muted mb-15 mt-5">Made from a crisp, medium-weight cotton and designed for a tailored</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/women-3.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Grey Sweater</h6>
				<p class="text-muted mb-15 mt-5">Round neck, Short sleeve, Basic plain t-shirt, Slim fit, Color : Grey</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/products/product-6.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">Edit</a>
					</span>
				</div>
			</div>

			<div class="caption text-left">
				<h6 class="text-semibold no-margin">Cream Sweater</h6>
				<p class="text-muted mb-15 mt-5">Round neck, Short sleeve, Basic plain t-shirt, Slim fit, Color : Black</p>
			</div>
		</div>
	</div>
</div>
@endsection