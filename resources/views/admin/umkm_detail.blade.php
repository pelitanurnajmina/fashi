@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-lg-3 col-md-6">
		<div class="thumbnail">
			<div class="thumb thumb-slide">
				<img src="img/gibpokki.png" alt="">
			</div>
		
	    	<div class="caption text-center">
	    		<h6 class="text-semibold no-margin">Gibpokki
	    			<small class="display-block">ayunurfajrina30@gmail.com</small>
	    			<small class="display-block mt-10">Mendaftar sejak 06 Juni 2020</small>
	    		</h6>
	    	</div>
    	</div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-flat">
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th colspan="2" class="text-bold text-center">DATA UMKM</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>Nama UMKM</th>
							<td>Gibpokki</td>
						</tr>
						<tr>
							<th>Kategori</th>
							<td>Kuliner</td>
						</tr>
						<tr>
							<th>Alamat</th>
							<td>Sukawarna Bandung</td>
						</tr>
						<tr>
							<th>Telephone</th>
							<td>0899817171</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>ayunurfajrina30@gmail.com</td>
						</tr>
						<tr>
							<th>Berdiri Sejak</th>
							<td>2020</td>
						</tr>
						<tr>
							<th>Website</th>
							<td><a href="">https://gibpokki.com</a></td>
						</tr>
						<tr>
							<th>Lampiran</th>
							<td><a href=""><i class="icon-file-download mr-5"></i> Download</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection