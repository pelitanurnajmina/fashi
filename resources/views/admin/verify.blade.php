@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Verify UMKM</h5>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>

			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th class="text-center">Address</th>
							<th class="text-center">Telephone</th>
							<th class="text-center">Email</th>
							<th class="text-center">Since</th>
							<th class="text-center">Website</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Tropic Collection</td>
							<td class="text-center">Ciberem Cimahi</td>
							<td class="text-center">082992722</td>
							<td class="text-center">feby@gmail.com</td>
							<td class="text-center">2019</td>
							<td class="text-center"><a href="">https://collection.com</a></td>
							<td class="text-center">
								<i class="icon-checkmark4 text-primary" style="cursor: pointer;"></i>
								<i class="icon-cross2 text-danger ml-15" style="cursor: pointer;"></i>
								<a href="umkm_detail"><i class="icon-more text-success ml-15" style="cursor: pointer;"></i></a>
							</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Gibpokki</td>
							<td class="text-center">Sukawarna Bandung</td>
							<td class="text-center">0887463111</td>
							<td class="text-center">ayunurfajrina@gmail.com</td>
							<td class="text-center">2020</td>
							<td class="text-center"><a href="">https://gibpokki.com</a></td>
							<td class="text-center">
								<i class="icon-checkmark4 text-primary" style="cursor: pointer;"></i>
								<i class="icon-cross2 text-danger ml-15" style="cursor: pointer;"></i>
								<i class="icon-more text-success ml-15" style="cursor: pointer;"></i>
							</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Hanna Mochi</td>
							<td class="text-center">Pesantren Cimahi</td>
							<td class="text-center">087613619</td>
							<td class="text-center">hanna@gmail.com</td>
							<td class="text-center">2018</td>
							<td class="text-center"><a href="">https://neng-mochi.com</a></td>
							<td class="text-center">
								<i class="icon-checkmark4 text-primary" style="cursor: pointer;"></i>
								<i class="icon-cross2 text-danger ml-15" style="cursor: pointer;"></i>
								<i class="icon-more text-success ml-15" style="cursor: pointer;"></i>
							</td>
						</tr>
						<tr>
							<td>4</td>
							<td>Cindy Outfit</td>
							<td class="text-center">Cibabat Cimahi</td>
							<td class="text-center">0864129198</td>
							<td class="text-center">cindy@gmail.com</td>
							<td class="text-center">2018</td>
							<td class="text-center"><a href="">https://outfit-c.com</a></td>
							<td class="text-center">
								<i class="icon-checkmark4 text-primary" style="cursor: pointer;"></i>
								<i class="icon-cross2 text-danger ml-15" style="cursor: pointer;"></i>
								<i class="icon-more text-success ml-15" style="cursor: pointer;"></i>
							</td>
						</tr>
						<tr>
							<td>5</td>
							<td>Handsi</td>
							<td class="text-center">Gunung Batu</td>
							<td class="text-center">0865138141</td>
							<td class="text-center">fadhliqasthalan@gmail.com</td>
							<td class="text-center">2016</td>
							<td class="text-center"><a href="">https://handsi.com</a></td>
							<td class="text-center">
								<i class="icon-checkmark4 text-primary" style="cursor: pointer;"></i>
								<i class="icon-cross2 text-danger ml-15" style="cursor: pointer;"></i>
								<i class="icon-more text-success ml-15" style="cursor: pointer;"></i>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection