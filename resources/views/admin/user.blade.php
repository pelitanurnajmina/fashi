@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">List User</h5>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>

			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>NIK</th>
							<th>Name</th>
							<th class="text-center">Address</th>
							<th class="text-center">Telephone</th>
							<th class="text-center">Email</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>130037266620005</td>
							<td>Pelita Nur Najmina</td>
							<td class="text-center">Dakota Bandung</td>
							<td class="text-center">087655291862</td>
							<td class="text-center">pelitanurnajmina27@gmail.com</td>
							<td class="text-center">
								<i class="icon-pencil text-warning" style="cursor: pointer;"></i>
								<i class="icon-trash text-danger ml-15" style="cursor: pointer;"></i>
								<a href="user_detail"><i class="icon-more text-success ml-15" style="cursor: pointer;"></i></a>
							</td>
						</tr>
						<tr>
							<td>1</td>
							<td>130037266620005</td>
							<td>Ayu Nur Fajrina</td>
							<td class="text-center">Sukawarna Bandung</td>
							<td class="text-center">08388392872</td>
							<td class="text-center">ayunurfajrina30@gmail.com</td>
							<td class="text-center">
								<i class="icon-pencil text-warning" style="cursor: pointer;"></i>
								<i class="icon-trash text-danger ml-15" style="cursor: pointer;"></i>
								<a href="user_detail"><i class="icon-more text-success ml-15" style="cursor: pointer;"></i></a>
							</td>
						</tr>
						<tr>
							<td>2</td>
							<td>130038258620001</td>
							<td>Fadhli Nur Qasthalan</td>
							<td class="text-center">Gunung Batu Bandung</td>
							<td class="text-center">0886298121</td>
							<td class="text-center">fadhliqasthalan@gmail.com</td>
							<td class="text-center">
								<i class="icon-pencil text-warning" style="cursor: pointer;"></i>
								<i class="icon-trash text-danger ml-15" style="cursor: pointer;"></i>
								<a href="user_detail"><i class="icon-more text-success ml-15" style="cursor: pointer;"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection