@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-lg-3 col-md-6">
		<div class="thumbnail">
			<div class="thumb thumb-slide">
				<img src="img/pelita.jpg" alt="">
			</div>
		
	    	<div class="caption text-center">
	    		<h6 class="text-semibold no-margin">PELITA NUR NAJMINA
	    			<small class="display-block">pelitanurnajmina27@gmail.com</small>
	    			<small class="display-block mt-10">Terdaftar sejak 03 Juni 2020</small>
	    		</h6>
	    	</div>
    	</div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-flat">
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th colspan="2" class="text-bold text-center">DATA KTP</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>Nama Lengkap</th>
							<td>Pelita Nur Najmina</td>
						</tr>
						<tr>
							<th>Tempat Tanggal Lahir</th>
							<td>Bandung, 27 Desember 1997</td>
						</tr>
						<tr>
							<th>Jenis Kelamin</th>
							<td>Perempuan</td>
						</tr>
						<tr>
							<th>Golongan Darah</th>
							<td>AB</td>
						</tr>
						<tr>
							<th>Alamat</th>
							<td>Jalan Mentor Gg Hambali No 106</td>
						</tr>
						<tr>
							<th>RT/RW</th>
							<td>001/007</td>
						</tr>
						<tr>
							<th>Kelurahan</th>
							<td>Sukaraja</td>
						</tr>
						<tr>
							<th>Kecamatan</th>
							<td>Cicendo</td>
						</tr>
						<tr>
							<th>Agama</th>
							<td>Islam</td>
						</tr>
						<tr>
							<th>Status Perkawinan</th>
							<td>Belum Kawin</td>
						</tr>
						<tr>
							<th>Kewarganegaraan</th>
							<td>WNI</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection