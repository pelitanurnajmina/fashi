@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/bengkel.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Bengkel Motor Padasuka</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/kopi.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Kedai Kopi ABC</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/kue.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Kue Balok Mang Didin</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/bakso.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Bakso Istigfar</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/rm.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Rumah Makan Ibu Imas</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/mg.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Kaos Margin Dream</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/cell.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Ayub Cell</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/shoes.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Ibrahim Shoes</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="thumbnail">
			<div class="thumb">
				<img src="img/vendor/hijab.jpg" alt="">
				<div class="caption-overflow">
					<span>
						<a href="#" class="btn btn-info">View</a>
					</span>
				</div>
			</div>

			<div class="caption text-center">
				<h6 class="text-semibold no-margin">Nur Hijab</h6>
			</div>
		</div>
	</div>
</div>
@endsection