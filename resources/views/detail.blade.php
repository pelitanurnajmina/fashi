@extends('layouts.app')

@section('content')
<section class="blog-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="blog-details-inner">
                    <div class="blog-detail-title">
                        <h2>Margin Dream</h2>
                        <p>Pakaian <span>- Since 2019 | 082892103710 | margindream@gmail.com</span></p>
                    </div>
                    <div class="blog-large-pic">
                        <img src="img/products/banner.jpg" alt="">
                    </div>
                    <div class="blog-detail-desc">
                        <p class="text-justify">
                            Anak muda memang tidak bisa dijauhkan dari kata fashion. Bagi mereka fashion adalah hal yang wajib diperhatikan dalam setiap penampilan mereka. Pilihan dalam mengembangkan fashion anak muda saat ini adalah dengan mulai menjamurnya distro-distro di Indonesia.

                            Hal ini karena distro merupakan pilihan fashion yang mampu memanjakan anak muda masa kini, mulai dari sepatu, celana jeans, kaos, jaket, hingga topi dan aksesoris lainnya.

                            Distro sudah menjadi sebuah fenomena baru yang hadir khususnya di kota-kota besar di Indonesia. Keberadaan distro menjadi sebuah trend setter untuk menghadirkan gaya tatanan busana ala remaja dengan segala macam keunikan yang hadir mengiringinya.

                            Distro sendiri umumnya berisi kaos-kaos yang berasal dari perusahaan clothing. Lalu apa sih bedanya perusahaan clothing dan distro? Bagaiman sejarah keduanya hingga saat ini bisnis distro begitu berkembang?
                        </p>
                    </div>
                    <div class="blog-more">
                        <div class="row">
                            <div class="col-sm-4">
                                <img src="img/products/product-1.jpg" alt="">
                            </div>
                            <div class="col-sm-4">
                                <img src="img/products/product-2.jpg" alt="">
                            </div>
                            <div class="col-sm-4">
                                <img src="img/products/product-3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <p class="text-justify">
                        Clothing adalah istilah untuk menyebut perusahaan pembuat kaos.  Istilah lengkapnya adalah perusahaan clothing yang membuat pakaian berupa kaos di bawah produksi dengan merk sendiri.

                        Kaos yang menjadi produksi utama perusahaan clothing pun kemudian berkembang ke segala kebutuhan sekunder manusia yakni style mereka. Perusahaan clothing mulai memproduksi jaket, dompet, longpant, kaos polo, topi, tas, aksesoris seperti pin gelang juga ikut diproduksi. 
                    </p>
                    <div class="tag-share">
                        <div class="details-tag">
                            {{-- <ul>
                                <li><i class="fa fa-tags"></i></li>
                                <li>Travel</li>
                                <li>Beauty</li>
                                <li>Fashion</li>
                            </ul> --}}
                        </div>
                        <div class="blog-share">
                            <span>Share:</span>
                            <div class="social-links">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="posted-by mt-4">
                        <div class="pb-pic">
                            <img src="img/man.png" alt="" width="90px">
                        </div>
                        <div class="pb-text">
                            <a href="#">
                                <h5>Ayub Wahyu</h5>
                            </a>
                            <p>Kualitas dari setiap produk memang sangat oke</p>
                        </div>
                    </div>           

                    <div class="posted-by mt-4">
                        <div class="pb-pic">
                            <img src="img/man-2.png" alt="" width="90px">
                        </div>
                        <div class="pb-text">
                            <a href="#">
                                <h5>Ibrahim Ibadurrahman</h5>
                            </a>
                            <p>Model dari setiap baju dan lainnya sangatlah menarik</p>
                        </div>
                    </div>    

                    <div class="posted-by mt-4" id="post-pelita" style="display: none;">
                        <div class="pb-pic">
                            <img src="img/woman.png" alt="" width="90px">
                        </div>
                        <div class="pb-text">
                            <a href="#">
                                <h5>Pelita Nur Najmina</h5>
                            </a>
                            <p>Saya beri bintang 5 untuk vendor ini</p>
                        </div>
                    </div>
                

                    <div class="leave-comment">
                        <h4>Leave A Comment</h4>
                        <form action="#" class="comment-form">
                            <div class="row">
                                {{-- <div class="col-lg-6">
                                    <input type="text" placeholder="Name">
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" placeholder="Email">
                                </div> --}}
                                <div class="col-lg-12">
                                    <textarea placeholder="Messages" id="text"></textarea>
                                    <button type="button" class="site-btn" onclick="message()">Send message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')
<script type="text/javascript">
    function message() {
        $('#post-pelita').show();

        document.getElementById('text').value = '';
    }
</script>
@endpush