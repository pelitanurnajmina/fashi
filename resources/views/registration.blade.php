@extends('layouts.app')

@section('content')
<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <a href="#"><i class="fa fa-home"></i> Home</a>
                    <span>Registration</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Form Section Begin -->

<!-- Register Section Begin -->
<div class="register-login-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="login-form">
                    <h2>Registration</h2>
                    <form action="#" autocomplete="off">
                        <div class="group-input">
                            <label>NIK</label>
                            <input type="text">
                        </div>
                        <div class="group-input">
                            <label>Email</label>
                            <input type="text" name="">
                        </div>
                        <div class="group-input">
                            <label>Password</label>
                            <input type="password">
                        </div>
                        <button type="button" data-target="#modal-registration" data-toggle="modal" class="site-btn login-btn">Sign In</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Register Form Section End -->

 <div id="modal-registration" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3 text-center">
                            <img src="img/checklist.png" width="150px"> <br> <br>
                            Pendaftaran Berhasil <br> <br> 
                            <a href="home" type="submit" class="site-btn login-btn">Back to Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection