<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::view('home', 'home');
Route::view('detail', 'detail');
Route::view('registration', 'registration');

Route::view('verify', 'admin.verify');
Route::view('dashboard', 'admin.dashboard');
Route::view('list', 'admin.list');
Route::view('user', 'admin.user');
Route::view('user_detail', 'admin.user_detail');
Route::view('umkm_detail', 'admin.umkm_detail');

Route::view('dashboard_umkm', 'umkm.dashboard');
Route::view('product', 'umkm.product');